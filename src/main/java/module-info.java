module com.kevinread.syslog4j {
    requires static commons.pool;
    requires static org.joda.time;
    requires static com.sun.jna;
    exports org.productivity.java.syslog4j;
    exports org.productivity.java.syslog4j.util;
    exports org.productivity.java.syslog4j.server;
    exports org.productivity.java.syslog4j.server.net;
    exports org.productivity.java.syslog4j.server.net.tcp;
    exports org.productivity.java.syslog4j.server.net.udp;
}